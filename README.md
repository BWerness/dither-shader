# README #

This is the processing experimental code that was used to develop the shader which has been modified to be used in The Return of the Obra Dinn. It is designed to produce results similar to those obtained from error diffusion type dithering algorithms in a way that admits massive parallelization in a GPU.
